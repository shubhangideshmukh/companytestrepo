import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public ajaxSettings: object;
  public view: string;
  public hostUrl: string = 'https://ej2-aspcore-service.azurewebsites.net/';
  public ngOnInit(): void {
      this.ajaxSettings = {
          url: this.hostUrl + 'api/FileManager/FileOperations',
          getImageUrl: this.hostUrl + 'api/FileManager/GetImage',
          uploadUrl: this.hostUrl + 'api/FileManager/Upload',
          downloadUrl: this.hostUrl + 'api/FileManager/Download'
      };
      // Initial view of File Manager is set to details view
      console.log("ajaxsetting",this.ajaxSettings);
      this.view = "Details";
  };
  // File Manager's created event function
  onCreate(args: any) {
    debugger
    // console.log
      console.log("File Manager has been created successfully",args);
  }
}